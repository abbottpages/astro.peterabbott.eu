#!/usr/bin/env raku
use v6;
use DateTime::Format;

# Get the current date
my $dt = DateTime.now;

# Function to get the correct ordinal suffix
sub ordinal-suffix(Int $day) {
    given $day % 100 {
        when 11..13 { "th" }
        default {
            given $day % 10 {
                when 1  { "st" }
                when 2  { "nd" }
                when 3  { "rd" }
                default { "th" }
            }
        }
    }
}

# Create the formatted date string
my $current_date = sprintf(
    "%s%s - %d",
    strftime('%B %d', $dt),
    ordinal-suffix($dt.day),
    $dt.year
);

# Path to the HTML file
my $file_path = "public/index.html";

# Check if the file exists before attempting to read it
if not $file_path.IO.e {
    die "File '$file_path' does not exist.";
}

# Read and process the file
try {
    # Attempt to read the file
    my $content = try {
        $file_path.IO.slurp;
    }
    CATCH {
        default {
            die "Failed to read file '$file_path': $_";
        }
    };

    # Regex to match the date string
    my $search = /'Peter'.*? 'Abbott' \s*"-"\s* 'Updated:' \s* (.*?) \s* \((.*?)\)/;

    if $content ~~ $search {
        my $existing_date = $0.Str.trim;
        if $existing_date ne $current_date {
            # Substitute the date
            $content.=subst($existing_date, $current_date, :g);
            $file_path.IO.spurt($content);
            say "Date updated to: $current_date";
        } else {
            say "Date is current: $current_date";
        }
    } else {
        die "Update pattern not found in file";
    }
}
CATCH {
    default {
        die "Error: $_";
    }
}
